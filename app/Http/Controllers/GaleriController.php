<?php

namespace App\Http\Controllers;

use App\Models\Galeri;
use App\Models\Galeriimage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GaleriController extends Controller
{
    public function index(Request $request)
    {
        if ($request->tahun) {
            $galeri = Galeri::where(DB::raw('YEAR(tanggal)'), $request->tahun)->latest()->get();
        } else {    
            $galeri = Galeri::where(DB::raw('YEAR(tanggal)'), now())->latest()->get();
        }

        $tahun = DB::table('galeri')->select(DB::raw('YEAR(tanggal) as tahun'))->orderBy(DB::raw('YEAR(tanggal)'), 'desc')->groupBy(DB::raw('YEAR(tanggal)'))->pluck('tahun');
        
        $data = [
            'tahun' => $tahun,
            'galeri' => $galeri
        ];
        return view('galeri.galeri', $data);
    }

    public function create()
    {
        return view('galeri.create-galeri');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required|max:255',
            'tanggal' => 'required',
            'deskripsi' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('galeri/create')->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $album = Galeri::create([
            'judul' => $request->judul,
            'tanggal' => $request->tanggal,
            'deskripsi' => $request->deskripsi,
        ]);

        if ($request->file('foto')) {
            foreach ($request->file('foto') as $file) {
                $nama_foto = date('Ymdhis') . '_' . $file->getClientOriginalName();
                $file->move('admin/images/galeri/', $nama_foto);
            
                $data = array(
                    'galeri_id' => $album->id,
                    'foto' => $nama_foto
                );
                Galeriimage::create($data);
            }
        }

        return redirect('/galeri')->with('success', 'Galeri baru berhasil dibuat!');
    }

    public function show($id)
    {
        $galeri = Galeri::find($id);
        $data = [
            'galeri' => $galeri,
            'photos' => Galeriimage::where('galeri_id', $galeri->id)->get()
        ];
        return view('galeri.detail', $data);
    }

    public function edit($id)
    {
        $galeri = Galeri::find($id);
        $data = [
            'galeri' => $galeri,
            'photos' => Galeriimage::where('galeri_id', $galeri->id)->get()
        ];
        return view('galeri.edit-galeri', $data);
    }

    public function update(Request $request, $id)
    {
        $album = Galeri::find($id);

        $validator = Validator::make($request->all(), [
            'judul' => 'required|max:255',
            'tanggal' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $album->update([
            'judul' => $request->judul,
            'tanggal' => $request->tanggal,
            'deskripsi' => $request->deskripsi,
        ]);

        // Pengkondisian upload gambar
        if ($request->file('foto')) {
            Galeriimage::where('galeri_id', $album->id)->delete();

            foreach ($request->file('foto') as $file) {
                $nama_foto = date('Ymdhis') . '_' . $file->getClientOriginalName();
                $file->move('admin/images/galeri/', $nama_foto);
                if ($request->oldImage) {
                    foreach ($request->oldImage as $fotolama) {
                        if(file_exists('admin/images/galeri/'. $fotolama)) {
                            unlink('admin/images/galeri/'. $fotolama);
                        }
                    }
                }
            
                $data = array(
                    'galeri_id' => $album->id,
                    'foto' => $nama_foto
                );
                Galeriimage::create($data);
            }
        }

        return redirect('/galeri')->with('success', 'Galeri berhasil diubah!');
    }

    public function delete($id)
    {
        $album = Galeri::find($id);
        if ($album->image) {
            foreach ($album->image as $galeri) {
                if(file_exists('admin/images/galeri/'. $galeri->foto)) {
                    unlink('admin/images/galeri/'. $galeri->foto);
                }
            }
        }
        Galeriimage::where('galeri_id', $album->id)->delete();
        $album->delete();

        return redirect('/galeri')->with('success', 'Galeri berhasil dihapus!');
    }
}
