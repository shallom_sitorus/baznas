<?php

namespace App\Http\Controllers;
use App\Models\Emas;
use App\Models\Artikel;
use App\Models\Penerimazis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $artikel = Artikel::where(DB::raw('YEAR(created_at)'), now());
        $zis = Penerimazis::where(DB::raw('tahun'), now())->get();
        $data = [
            'artikel' => $artikel,
            'zakat' => $zis->pluck('zakat'),
            'infaq_terikat' => $zis->pluck('infaq_terikat'),
            'infaq_umum' => $zis->pluck('infaq_umum'),
            'total' => $zis->pluck('total'),
            'bulan' => $zis->pluck('bulan'),
            'tahun' => date('Y'),
            'emas' => Emas::find('1'),
        ];

        return view('dashboard', $data);
    }
}
