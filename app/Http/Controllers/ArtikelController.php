<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Cviebrock\EloquentSluggable\Services\SlugService;

class ArtikelController extends Controller
{
    public function index(Request $request)
    {
        if ($request->tahun) {
            $artikel = Artikel::where(DB::raw('YEAR(created_at)'), $request->tahun)->get();
        } else {    
            $artikel = Artikel::where(DB::raw('YEAR(created_at)'), now())->get();
        }

        $tahun = DB::table('artikel')->select(DB::raw('YEAR(created_at) as tahun'))->orderBy(DB::raw('YEAR(created_at)'), 'desc')->groupBy(DB::raw('YEAR(created_at)'))->pluck('tahun');
        
        $data = [
            'tahun' => $tahun,
            'artikel' => $artikel

        ];
        // dd($tahun);
        return view('artikel.artikel', $data);
    }

    public function show(Artikel $artikel)
    {
        return view('artikel.detail', [
            'artikel' => $artikel
        ]);
    }

    public function create()
    {
        return view('artikel.create-artikel');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required|max:255',
            'slug' => 'required|unique:artikel',
            'image' => 'image|file|max:1024',
            'body' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('artikel/create')->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('image')) {
            $nama_gambar = date('Ymdhis') . '_' . $request->file('image')->getClientOriginalName();
            $request->file('image')->move('admin/images/artikel/', $nama_gambar);
        } else {
            $nama_gambar = '';
        }

        Artikel::create([
            'judul' => $request->judul,
            'slug' => $request->slug,
            'image' => $nama_gambar,
            'body' => $request->body,
            'user_id' => $request->user_id
        ]);

        // dd($request);
        return redirect('/artikel')->with('success', 'Artikel baru berhasil dibuat!');
    }

    public function edit(Artikel $artikel)
    {
        return view('artikel.edit-artikel', [
            'artikel' => $artikel
        ]);
    }
    
    public function update(Request $request, Artikel $artikel)
    {
        $rules = [
            'judul' => 'required|max:255',
            'image' => 'image|file|max:1024',
            'body' => 'required'
        ];

        if($request->slug != $artikel->slug) {
            $rules['slug'] = 'required|unique:artikel';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        // Pengkondisian upload gambar
        if ($request->file('image')) {
            $nama_gambar = date('Ymdhis') . '_' . $request->file('image')->getClientOriginalName();
            $request->file('image')->move('admin/images/artikel/', $nama_gambar);
            if ($request->oldImage) {
                unlink('admin/images/artikel/'. $request->oldImage);
            }
        } else {
            $nama_gambar = $request->oldImage;
        }

        Artikel::where('id', $artikel->id)
            ->update([
                'judul' => $request->judul,
                'slug' => $request->slug,
                'image' => $nama_gambar,
                'body' => $request->body,
                'user_id' => $request->user_id
            ]);

        return redirect('/artikel')->with('success', 'Artikel berhasil diubah!');
    }

    public function delete(Artikel $artikel)
    {
        if($artikel->image) {
            unlink('admin/images/artikel/'. $artikel->image);
        }
        Artikel::destroy($artikel->id);
        return redirect('/artikel')->with('success', 'Artikel berhasil dihapus!');
    }

    public function checkSlug(Request $request)
    {
        $slug = SlugService::createSlug(Artikel::class, 'slug', $request->judul);
        return response()->json(['slug' => $slug]);
    }

    public function attachStore(Request $request)
    {
        $file = $request->file('attachment');

        $path = $file->store('public/attachments');

        $url = Storage::url($path);

        $attachment = [
            'url' => $url,
            'name' => $file->getClientOriginalName(),
            'size' => $file->getSize(),
            'type' => $file->getMimeType()
        ];

        return response()->json($attachment);
    }
}
