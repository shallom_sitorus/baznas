<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $data = [
            'users' => User::find(Auth::user()->id)
        ];
        
        return view('user.profile', $data);
    }

    public function edit()
    {
        $data = [
            'users' => User::find(Auth::user()->id)
        ];
        
        return view('user.edit-profile', $data);
    }

    public function update(Request $request, $id)
    {
        // dd($request);
        $user = User::find(Auth::user()->id);

        $rules = [
            'name'     => 'required|max:255',
            'password' => 'required|min:8|max:255',
            'foto'     => 'image|file|max:1024'
        ];

        if($request->email != $user->email) {
            $rules['email'] = 'unique:users|required';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        //Pengkondisian password
        if($request->password != $request->password_lama) {
            $passwordHash = Hash::make($request->password);
        } else {
            $passwordHash = $request->password_lama;
        }

        if ($request->file('foto')) {
            $nama_gambar = date('Ymdhis') . '_' . $request->file('foto')->getClientOriginalName();
            $request->file('foto')->move('admin/images/user/', $nama_gambar);
            if ($request->oldImage) {
                unlink('admin/images/user/'. $request->oldImage);
            }
        } else {
            $nama_gambar = $request->oldImage;
        }

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $passwordHash,
            'foto' => $nama_gambar
        ]);

        return redirect('/profile')->with('success', 'Profile berhasil diubah!');
    }

}
