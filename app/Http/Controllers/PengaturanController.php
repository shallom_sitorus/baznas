<?php

namespace App\Http\Controllers;

use App\Models\Emas;
use App\Models\Kontak;
use App\Models\Tentang;
use App\Models\Rekening;
use App\Models\Sosmed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PengaturanController extends Controller
{
    public function hargaEmas()
    {
        $data = [
            'emas' => Emas::all()
        ];
        // dd($data);
        return view('kalkulator.pengaturan', $data);
    }

    public function updateHargaEmas(Request $request, $id)
    {
        $emas = Emas::find($id);

        $rules = [
            'harga' => 'required|numeric'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        $emas->update([
            'harga' => $request->harga,
        ]);

        return redirect('/pengaturan/kalkulator')->with('success', 'Harga Emas berhasil diupdate!');
    }

    public function rekening()
    {
        $data = [
            'rekening' => Rekening::all()
        ];
        // dd($data);
        return view('rekening.index', $data);
    }
    public function createRekening()
    {
        return view('rekening.create');
    }

    public function storeRekening(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bank' => 'required',
            'no_rekening' => 'required',
            'nama_rekening' => 'required',
            'logo' => 'image|file|max:1024',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('logo')) {
            $nama_gambar = date('Ymdhis') . '_' . $request->file('logo')->getClientOriginalName();
            $request->file('logo')->move('admin/images/rekening/', $nama_gambar);
        } else {
            $nama_gambar = '';
        }

        Rekening::create([
            'bank' => $request->bank,
            'no_rekening' => $request->no_rekening,
            'nama_rekening' => $request->nama_rekening,
            'logo' => $nama_gambar,
        ]);

        // dd($request);
        return redirect('/pengaturan/rekening')->with('success', 'Rekening baru berhasil dibuat!');
    }

    public function editRekening($id)
    {
        return view('rekening.edit', [
            'rek' => Rekening::find($id)
        ]);
    }

    public function updateRekening(Request $request, $id)
    {
        // dd($request);
        $rules = [
            'bank' => 'required',
            'no_rekening' => 'required',
            'nama_rekening' => 'required',
            'logo' => 'image|file|max:1024',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        // Pengkondisian upload gambar
        if ($request->file('logo')) {
            $nama_gambar = date('Ymdhis') . '_' . $request->file('logo')->getClientOriginalName();
            $request->file('logo')->move('admin/images/rekening/', $nama_gambar);
            if ($request->oldImage) {
                unlink('admin/images/rekening/'. $request->oldImage);
            }
        } else {
            $nama_gambar = $request->oldImage;
        }

        Rekening::where('id', $id)
            ->update([
                'bank' => $request->bank,
                'no_rekening' => $request->no_rekening,
                'nama_rekening' => $request->nama_rekening,
                'logo' => $nama_gambar,
            ]);

        return redirect('/pengaturan/rekening')->with('success', 'Rekening berhasil diubah!');
    }

    public function deleteRekening($id)
    {
        $rek = Rekening::find($id);
        if($rek->logo) {
            unlink('admin/images/rekening/'. $rek->logo);
        }
        Rekening::destroy($id);
        return redirect('/pengaturan/rekening')->with('success', 'Rekening berhasil dihapus!');
    }

    public function sosmed()
    {
        return view('sosmed.index', [
            'sosmed' => Sosmed::all()
        ]);
    }

    public function storeSosmed(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_sosmed' => 'required|unique:sosmed',
            'link' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        Sosmed::create([
            'nama_sosmed' => $request->nama_sosmed,
            'link' => $request->link,
        ]);

        // dd($request);
        return redirect('/pengaturan/sosmed')->with('success', 'Sosial Media baru berhasil dibuat!');
    }

    public function updateSosmed(Request $request, $id)
    {
        // dd($request);
        $sosmed = Sosmed::find($id);

        $rules = [
            'link' => 'required',
        ];

        if($request->nama_sosmed != $sosmed->nama_sosmed) {
            $rules['nama_sosmed'] = 'required|unique:sosmed';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        Sosmed::where('id', $id)
            ->update([
                'nama_sosmed' => $request->nama_sosmed,
                'link' => $request->link,
            ]);

        return redirect('/pengaturan/sosmed')->with('success', 'Sosial Media berhasil diupdate!');
    }

    public function deleteSosmed($id)
    {
        Sosmed::destroy($id);
        return redirect('/pengaturan/sosmed')->with('success', 'Sosial Media berhasil dihapus!');
    }

    public function kontak()
    {
        return view('kontak.index', [
            'kontak' => Kontak::all()
        ]);
    }

    public function storeKontak(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_kontak' => 'required|unique:kontak',
            'kontak' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        Kontak::create([
            'nama_kontak' => $request->nama_kontak,
            'kontak' => $request->kontak,
        ]);

        // dd($request);
        return redirect('/pengaturan/kontak')->with('success', 'Sosial Media baru berhasil dibuat!');
    }

    public function updateKontak(Request $request, $id)
    {
        // dd($request);
        $sosmed = Sosmed::find($id);

        $rules = [
            'kontak' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        Kontak::where('id', $id)
            ->update([
                'kontak' => $request->kontak,
            ]);

        return redirect('/pengaturan/kontak')->with('success', 'Kontak berhasil diupdate!');
    }

    public function deleteKontak($id)
    {
        Kontak::destroy($id);
        return redirect('/pengaturan/kontak')->with('success', 'Kontak berhasil dihapus!');
    }

    public function tentangkami()
    {
        $data = [
            'tentang' => Tentang::all()
        ];
        return view('tentang_kami.profil-baznas', $data);
    }

    public function editProfil($id)
    {
        return view('tentang_kami.edit-profil', [
            'tentang' => Tentang::find($id)
        ]);
    }

    public function updateProfil(Request $request, $id)
    {
        $rules = [
            'profil' => 'required',
            'gambarprofil' => 'image|file|max:2048',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        // Pengkondisian upload gambar
        if ($request->file('gambarprofil')) {
            $nama_gambar = date('Ymdhis') . '_' . $request->file('gambarprofil')->getClientOriginalName();
            $request->file('gambarprofil')->move('admin/images/tentang/', $nama_gambar);
            if ($request->oldImage) {
                unlink('admin/images/tentang/'. $request->oldImage);
            }
        } else {
            $nama_gambar = $request->oldImage;
        }

        Tentang::where('id', $id)
            ->update([
                'profil' => $request->profil,
                'gambarprofil' => $nama_gambar,
            ]);

        return redirect('/pengaturan/tentangkami')->with('success', 'Profil berhasil diubah!');
    }

    public function editVimi($id)
    {
        return view('tentang_kami.edit-vimi', [
            'tentang' => Tentang::find($id)
        ]);
    }

    public function updateVimi(Request $request, $id)
    {
        $rules = [
            'visi' => 'required',
            'misi' => 'required',
            'gambarvimi' => 'image|file|max:2048',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        // Pengkondisian upload gambar
        if ($request->file('gambarvimi')) {
            $nama_gambar = date('Ymdhis') . '_' . $request->file('gambarvimi')->getClientOriginalName();
            $request->file('gambarvimi')->move('admin/images/tentang/', $nama_gambar);
            if ($request->oldImage) {
                unlink('admin/images/tentang/'. $request->oldImage);
            }
        } else {
            $nama_gambar = $request->oldImage;
        }

        Tentang::where('id', $id)
            ->update([
                'visi' => $request->visi,
                'misi' => $request->misi,
                'gambarvimi' => $nama_gambar,
            ]);

        return redirect('/pengaturan/tentangkami')->with('success', 'Visi dan Misi berhasil diubah!');
    }

}
