<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use App\Models\Emas;
use App\Models\Galeri;
use App\Models\Penerimazis;
use App\Models\Rekening;
use App\Models\Sosmed;
use App\Models\Tentang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LandingController extends Controller
{
    public function __construct()
    {
        $instagram =  DB::table('sosmed')->where('nama_sosmed', 'Instagram')->value('link');
        $twitter =  DB::table('sosmed')->where('nama_sosmed', 'Twitter')->value('link');
        $whatsapp =  DB::table('sosmed')->where('nama_sosmed', 'Whatsapp')->value('link');
        $facebook =  DB::table('sosmed')->where('nama_sosmed', 'Facebook')->value('link');
        $youtube =  DB::table('sosmed')->where('nama_sosmed', 'Youtube')->value('link');
        $telpon =  DB::table('kontak')->where('nama_kontak', 'Telepon')->value('kontak');
        $alamat =  DB::table('kontak')->where('nama_kontak', 'Alamat')->value('kontak');
        $email =  DB::table('kontak')->where('nama_kontak', 'Email')->value('kontak');
        view()->share([
                'twitter' => $twitter,
                'whatsapp' => $whatsapp,
                'instagram' => $instagram,
                'facebook' => $facebook,
                'youtube' => $youtube,
                'telpon' => $telpon,
                'alamat' => $alamat,
                'email' => $email,
            ]);
    }
    public function index(Request $request)
    {
        if ($request->tahun) {
                $zis = Penerimazis::where(DB::raw('tahun'), $request->tahun)->get();
        } else {
                $zis = Penerimazis::where(DB::raw('tahun'), now())->get();
        }

        $data = [
            'zakat' => $zis->pluck('zakat'),
            'infaq_terikat' => $zis->pluck('infaq_terikat'),
            'infaq_umum' => $zis->pluck('infaq_umum'),
            'total' => $zis->pluck('total'),
            'bulan' => $zis->pluck('bulan'),
            'posts' => Artikel::latest()->get(),
            ];

        return view('beranda', $data);
    }

    public function posts()
    {
        return view('posts', [
            'posts' => Artikel::latest()->get()
        ]);
    }

    public function detailPost(Artikel $artikel)
    {
        return view('detailpost', [
            'artikel' => $artikel,
            'posts' => $artikel->latest()->get()
        ]);
    }

    public function tentang()
    {
        return view('tentang', [
            'tentang' => Tentang::all()
        ]);
    }

    public function visi()
    {
        return view('visi', [
            'tentang' => Tentang::all()
        ]);
    }

    public function galeri()
    {
        return view('galeri', [
            'galeri' => Galeri::latest()->get()
        ]);
    }

    public function penghasilan()
    {
        return view('kalkulator.penghasilan', [
            'emas' => Emas::all()
        ]);
    }

    public function maal()
    {
        return view('kalkulator.maal', [
            'emas' => Emas::all()
        ]);
    }

    public function zis(Request $request)
    {
        if ($request->tahun) {
                $zis = Penerimazis::where(DB::raw('tahun'), $request->tahun)->get();
        } else {
                $zis = Penerimazis::where(DB::raw('tahun'), now())->get();
        }

        $data = [
            'zakat' => $zis->pluck('zakat'),
            'infaq_terikat' => $zis->pluck('infaq_terikat'),
            'infaq_umum' => $zis->pluck('infaq_umum'),
            'total' => $zis->pluck('total'),
            'bulan' => $zis->pluck('bulan'),
            ];

        return view('zis', $data);
    }

    public function rekening()
    {
        return view('rekening.landing', [
            'rekening' => Rekening::all()
        ]);
    }

}
