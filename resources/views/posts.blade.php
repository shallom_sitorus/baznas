@extends('layouts.landingpage')

@section('content')
    <section class="news-detail-header-section text-center">
        <div class="section-overlay"></div>

        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-12">
                    <h1 class="text-white">Artikel dan Berita</h1>
                </div>

            </div>
        </div>
    </section>

    <section class="news-section section-padding">
        <div class="container">
            <div class="row">

                <div class="col-lg-7 col-12">
                    @foreach ($posts->take(2) as $post)
                        <div class="news-block mb-4">
                            <div class="news-block-top">
                                <a href="{{ url('/' . $post->slug) }}">
                                    <img src="{!! asset('/admin/images/artikel/' . $post->image) !!}" class="news-image img-fluid" alt="">
                                </a>
                            </div>

                            <div class="news-block-info">
                                <div class="d-flex mt-2">
                                    <div class="news-block-date">
                                        <p>
                                            <i class="bi-calendar4 custom-icon me-1"></i>
                                            {{ date('d M Y', strtotime($post->created_at)) }}
                                        </p>
                                    </div>

                                    <div class="news-block-author mx-5">
                                        <p>
                                            <i class="bi-person custom-icon me-1"></i>
                                            By {{ $post->author->name }}
                                        </p>
                                    </div>
                                </div>

                                <div class="news-block-title mb-2">
                                    <h4><a href="{{ url('/' . $post->slug) }}"
                                            class="news-block-title-link">{{ $post->judul }}</a>
                                    </h4>
                                </div>

                                <div class="news-block-body">
                                    <p>{{ Str::words(strip_tags($post->body), 30, '...') }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="col-lg-4 col-12 mx-auto mt-4 mt-lg-0">
                    <form class="custom-form search-form" action="#" method="post" role="form">
                        <input class="form-control" type="search" placeholder="Search" id="mySearch" aria-label="Search">

                        <button type="submit" class="form-control">
                            <i class="bi-search"></i>
                        </button>
                    </form>

                    <h5 class="mt-5 mb-3">Recent news</h5>

                    <div id="myCard">
                        @foreach ($posts->skip(2) as $post)
                            <div class="news-block news-block-two-col d-flex mt-4">
                                <div class="news-block-two-col-image-wrap">
                                    <a href="{{ url('/' . $post->slug) }}">
                                        <img src="{!! asset('/admin/images/artikel/' . $post->image) !!}" class="news-image img-fluid h-100"
                                            alt="">
                                    </a>
                                </div>

                                <div class="news-block-two-col-info">
                                    <div class="news-block-title mb-2">
                                        <h6><a href="{{ url('/' . $post->slug) }}"
                                                class="news-block-title-link">{{ Str::words($post->judul, 3, '...') }}</a>
                                        </h6>
                                    </div>

                                    <div class="news-block-date">
                                        <p>
                                            <i class="bi-calendar4 custom-icon me-1"></i>
                                            {{ date('d M Y', strtotime($post->created_at)) }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
