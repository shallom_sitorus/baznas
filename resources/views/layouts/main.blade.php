<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>BAZNAS Kabupaten Sukoharjo</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{!! asset('admin/vendors/feather/feather.css') !!}">
    <link rel="stylesheet" href="{!! asset('admin/vendors/ti-icons/css/themify-icons.css') !!}">
    <link rel="stylesheet" href="{!! asset('admin/vendors/css/vendor.bundle.base.css') !!}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{!! asset('admin/vendors/datatables.net-bs4/dataTables.bootstrap4.css') !!}">
    <link rel="stylesheet" href="{!! asset('admin/vendors/ti-icons/css/themify-icons.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('admin/js/select.dataTables.min.css') !!}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{!! asset('admin/css/vertical-layout-light/style.css') !!}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{!! asset('images/logo3.png') !!}" />
    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <!-- Selectpicker -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <!-- Trix Editor -->
    <link rel="stylesheet" type="text/css" href="{!! asset('/css/trix.css') !!}">
    <style>
        .attachment a img {
            width: 100%;
            height: auto;
        }

        #preview-container {
            display: flex;
            flex-wrap: wrap;
        }

        .preview-image {
            width: 200px;
            height: 200px;
            margin-right: 10px;
            margin-bottom: 10px;
            object-fit: cover;
        }

        .round-image {
        width: 200px;
        height: 175px;
        border-radius: 50%;
        object-fit: cover;
        }
    </style>
</head>

<body>
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        @include('partials.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_settings-panel.html -->
            @include('partials.settings-panel')
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            @include('partials.sidebar')
            <!-- partial -->
            <div class="main-panel">
                @include('sweetalert::alert')
                @yield('content')
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                {{-- @include('partials.footer') --}}
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- plugins:js -->
    <script src="{!! asset('admin/js/script.js') !!}"></script>
    <script>
        /* Mengambil inputan title dan slug */
        const judul = document.querySelector("#judul");
        const slug = document.querySelector("#slug");

        judul.addEventListener("change", function() {
            fetch("/artikel/checkSlug?judul=" + judul.value)
                .then((response) => response.json())
                .then((data) => (slug.value = data.slug));
        });

        /* Mengaktifkan fungsi attach file dalam trix */
        let editor = document.querySelector("trix-editor");
        let attachmentsInput = document.querySelector("#attachments");

        editor.addEventListener("trix-attachment-add", function(event) {
            if (event.attachment.file) {
                uploadAttachment(event.attachment);
            }
        });

        attachmentsInput.addEventListener("change", function(event) {
            Array.from(attachmentsInput.files).forEach(function(file) {
                uploadAttachment({
                    file: file
                });
            });
        });

        function uploadAttachment(attachment) {
            let form = new FormData();
            form.append("attachment", attachment.file);
            form.append("_token", "{{ csrf_token() }}");

            let xhr = new XMLHttpRequest();
            xhr.open("POST", "{{ route('attachments.store') }}", true);

            xhr.upload.addEventListener("progress", function(event) {
                let progress = (event.loaded / event.total) * 100;
                attachment.setUploadProgress(progress);
            });

            xhr.addEventListener("load", function(event) {
                if (xhr.status === 200) {
                    let response = JSON.parse(xhr.responseText);
                    attachment.setAttributes({
                        url: response.url,
                        href: response.url,
                    });

                    let input = document.createElement("input");
                    input.setAttribute("type", "hidden");
                    input.setAttribute("name", "attachments[]");
                    input.setAttribute("value", JSON.stringify(response));
                    document.querySelector("form").appendChild(input);
                }
            });

            xhr.send(form);
        }

        /* Melakukan fungsi Preview Image */
        function previewImage() {
            const image = document.querySelector("#image");
            const imgPreview = document.querySelector(".img-preview");

            imgPreview.style.display = "block";

            /* Perintah untuk ngambil data gambar nya */
            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            };
        }
    </script>
    <script>
        /* Mengoptimalkan fungsi choose file */
        var fileInput = document.getElementsByClassName("custom-file-input");

        for (var i = 0; i < fileInput.length; i++) {
            var input = fileInput[i];
            input.addEventListener("change", function(e) {
                var files = [];
                for (var i = 0; i < $(this)[0].files.length; i++) {
                    files.push($(this)[0].files[i].name);
                }
                $(this).next(".custom-file-label").html(files.join(", "));
            });
        }
    </script>

    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
        $(document).on("click", ".btn-delete", function(e) {
            e.preventDefault();
            const href = $(this).attr("action") || $(this).attr("href");

            Swal.fire({
                title: "Apakah Anda yakin?",
                text: "Data yang dihapus tidak dapat kembali",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#808080",
                confirmButtonText: "Ya, hapus!",
                cancelButtonText: "Batal",
            }).then((result) => {
                if (result.value) {
                    document.location.href = href;
                }
            });
        });
    </script>
    <!-- plugins:js -->
    <script src="{!! asset('admin/vendors/js/vendor.bundle.base.js') !!}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{!! asset('admin/vendors/chart.js/Chart.min.js') !!}"></script>
    <script src="{!! asset('admin/vendors/datatables.net/jquery.dataTables.js') !!}"></script>
    <script src="{!! asset('admin/vendors/datatables.net-bs4/dataTables.bootstrap4.js') !!}"></script>
    <script src="{!! asset('admin/js/dataTables.select.min.js') !!}"></script>
    <script>
        $(document).ready(function() {
            var tabel = $('#myTable').DataTable({
                // pagingType: 'numbers',
                // autoWidth: true,
                responsive: true,
                scrollX: true,
                dom: 't<"d-flex justify-content-between mt-2"lip>',
                language: {
                    // search: "",
                    searchPlaceholder: "Type here...",
                    paginate: {
                        previous: "<",
                        next: ">"
                    }
                }
            });

            $('#searchInput').keyup(function() {
                tabel.search($(this).val()).draw();
            });
        });
    </script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{!! asset('admin/js/off-canvas.js') !!}"></script>
    <script src="{!! asset('admin/js/hoverable-collapse.js') !!}"></script>
    <script src="{!! asset('admin/js/template.js') !!}"></script>
    <script src="{!! asset('admin/js/settings.js') !!}"></script>
    <script src="{!! asset('admin/js/todolist.js') !!}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{!! asset('admin/js/dashboard.js') !!}"></script>
    <script src="{!! asset('admin/js/Chart.roundedBarCharts.js') !!}"></script>
    <!-- End custom js for this page-->
    <!-- Trix Editor-->
    <script type="text/javascript" src="{!! asset('/js/trix.js') !!}"></script>

    <!-- Search Box -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        $(".selectpicker").selectpicker();
    </script>
</body>

</html>
