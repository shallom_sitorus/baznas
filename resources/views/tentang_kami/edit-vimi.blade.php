@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <a href="/pengaturan/tentangkami">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#6c757d"
                                    class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                                </svg>
                            </a>
                            <p class="card-description mx-2">Kembali</p>
                        </div>
                        <h4 class="card-title text-center">Form Edit Visi dan Misi</h4>
                        <form action="{{ url('/pengaturan/update-vimi/' . $tentang->id) }}" method="post" class="forms-sample" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label>Upload Gambar</label>
                                <input type="hidden" name="oldImage" value="{{ $tentang->gambarvimi }}">
                                @if ($tentang->gambarvimi)
                                    <img src="{{ asset('admin/images/tentang/' . $tentang->gambarvimi) }}"
                                        class="img-preview img-fluid mb-3 col-sm-5 d-block">
                                @else
                                    <img class="img-preview img-fluid mb-3 col-sm-5">
                                @endif
                                <img class="img-preview img-fluid mb-3 col-sm-5">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input @error('gambarvimi') is-invalid @enderror"
                                        name="gambarvimi" id="image" onchange="previewImage()">
                                    <label class="custom-file-label text-secondary"
                                        for="gambarvimi">{{ $tentang->gambarvimi ?? 'Choose file' }}</label>
                                </div>
                                @error('gambarvimi')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="visi">Visi</label>
                                <input id="visi" type="hidden" name="visi"
                                    value="{{ old('visi', $tentang->visi) }}">
                                <trix-editor input="visi"></trix-editor>
                                @error('visi')
                                    <small>
                                        <p class="text-danger">{{ $message }}</p>
                                    </small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="misi">Misi</label>
                                <input id="misi" type="hidden" name="misi"
                                    value="{{ old('misi', $tentang->misi) }}">
                                <trix-editor input="misi"></trix-editor>
                                @error('misi')
                                    <small>
                                        <p class="text-danger">{{ $message }}</p>
                                    </small>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button type="reset" class="btn btn-inverse-danger btn-fw">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
