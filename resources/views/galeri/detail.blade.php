@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="ml-xl-4 mt-3">
                            <div class="d-flex justify-content-between">
                                <p class="card-title">Detail Galeri</p>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="/galeri" type="button" class="btn btn-inverse-secondary btn-fw"><i
                                            class="ti-arrow-left"></i></a>
                                    <a href="/galeri/edit/{{ $galeri->id }}" type="button" id="edit-btn"
                                        class="btn btn-inverse-secondary btn-fw"><i class="ti-pencil-alt"></i></a>
                                    <a href="/galeri/delete/{{ $galeri->id }}" type="button"
                                        class="btn-delete btn btn-inverse-secondary btn-fw"><i class="ti-trash"></i></a>
                                </div>
                            </div>
                            <form class="form-sample mt-3" action="{{ url('/galeri/update/' . $galeri->id) }}"
                                method="post">
                                <p class="card-description" id="hidden-element" style="display:none">
                                    Ubah Informasi Galeri
                                </p>
                                @csrf
                                @method('put')
                                <div class="form-group row">
                                    <label class="col-sm-2" for="judul">Nama Album</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                            class="form-control form-control-sm @error('judul') is-invalid @enderror"
                                            id="judul" name="judul" placeholder="Nama Album"
                                            value="{{ old('judul', $galeri->judul) }}" readonly required>
                                        @error('judul')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="tanggal">Tanggal</label>
                                    <div class="col-sm-10">
                                        <input type="date"
                                            class="form-control form-control-sm @error('tanggal') is-invalid @enderror"
                                            id="tanggal" name="tanggal" placeholder="Tanggal"
                                            value="{{ old('tanggal', $galeri->tanggal) }}" readonly required>
                                        @error('tanggal')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="deskripsi">Deskripsi</label>
                                    <div class="col-sm-10">
                                        <textarea name="deskripsi" class="form-control" id="" rows="10"
                                            placeholder="Tuliskan deskripsi atau caption" readonly>{{ old('deskripsi', $galeri->deskripsi) }}</textarea>
                                        @error('deskripsi')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary mr-2" id="simpan-btn"
                                        style="display:none">Simpan</button>
                                    <a href="javascript:void(0)" class="btn btn-light" id="batal-btn" onclick="cancelEdit()"
                                        style="display:none">Batal</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="ml-xl-4 mt-3">
                            <div class="d-flex justify-content-between">
                                <p class="card-title">Foto-foto</p>
                            </div>
                            <div class="row">
                                @foreach ($photos as $gambar)
                                    <div class="col-md-3 mb-4 mb-lg-0 stretch-card transparent">
                                        <div class="card bg-dark text-white">
                                            <img src="{!! asset('admin/images/galeri/' . $gambar->foto) !!}" class="card-img rounded img-thumbnail img-fluid" alt="...">
                                            {{-- <div class="card-img-overlay"></div> --}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
