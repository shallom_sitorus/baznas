@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title">Galeri</h4>
                            <a href="/galeri/create"><button type="button" class="btn btn-success btn-sm">
                                    + Tambah
                                </button></a>
                        </div>
                        <div class="d-flex justify-content-between mt-3">
                            <form action="{{ url('/artikel') }}" method="GET">
                                @csrf
                                <div class="form-group d-flex">
                                    <label class="px-2 mt-2">Tahun:</label>
                                    <select class="form-control form-control-sm" name="tahun">
                                        @foreach ($tahun as $thn)
                                            <option value="{{ $thn }}" @if (request()->tahun)
                                                {{ request()->tahun == $thn ? 'selected' : '' }}
                                            @else
                                                {{ date('Y') == $thn ? 'selected' : '' }}
                                            @endif>{{ $thn }}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit" class="btn btn-sm btn-secondary text-white"
                                        style="margin-left: 5px;">Cari</button>
                                </div>
                            </form>
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label class="px-2 mt-2">Cari:</label>
                                    <input type="text" class="form-control form-control-sm" id="searchInput"
                                        placeholder="Cari di sini...">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @foreach ($galeri as $album)
                                <div class="col-md-3 mb-4 mb-lg-0 stretch-card transparent">
                                    <div class="card card-tale">
                                        @foreach ($album->image->take(1) as $gambar)
                                            <img src="{!! asset('/admin/images/galeri/' . $gambar->foto) !!}" class="card-img-top" alt="Album 3">
                                        @endforeach
                                        <div class="card-body">
                                            <h4 class="card-title text-light mb-2">{{ $album->judul }}</h4>
                                            <div class="d-flex justify-content-begin">
                                                <p>{{ date('d M Y', strtotime($album->tanggal)) }}</p>
                                            </div>
                                        </div>
                                        <div class="card-footer d-flex justify-content-center">
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <a href="/galeri/detail/{{ $album->id }}" type="button" class="btn btn-primary btn-fw"><i class="ti-eye"></i></a>
                                                <a href="/galeri/edit/{{ $album->id }}" type="button" class="btn btn-primary btn-fw"><i class="ti-pencil-alt"></i></a>
                                                <a href="/galeri/delete/{{ $album->id }}" type="button" class="btn-delete btn btn-primary btn-fw"><i class="ti-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
