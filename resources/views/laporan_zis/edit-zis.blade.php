@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <a href="/laporan_zis">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#6c757d"
                                    class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                                </svg>
                            </a>
                            <p class="card-description mx-2">Kembali</p>
                        </div>
                        <h4 class="card-title text-center pb-3">Form Edit Laporan Penerima ZIS</h4>
                        <form action="/laporan_zis/update/{{ $zis->id }}" method="post" class="forms-sample"
                            enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-lg-6">
                                    <label for="bulan">Bulan</label>
                                    <select class="selectpicker form-select @error('bulan') is-invalid @enderror"
                                        data-width="100%" name="bulan" data-live-search="true" data-size="6"
                                        title="-- Silahkan Pilih --" data-style="border border-gray rounded" required>
                                        <option>-- Silahkan Pilih --</option>
                                        <option value="Januari" {{ $zis->bulan == 'Januari' ? 'selected' : '' }}>Januari</option>
                                        <option value="Februari" {{ $zis->bulan == 'Februari' ? 'selected' : '' }}>Februari</option>
                                        <option value="Maret" {{ $zis->bulan == 'Maret' ? 'selected' : '' }}>Maret</option>
                                        <option value="April" {{ $zis->bulan == 'April' ? 'selected' : '' }}>April</option>
                                        <option value="Mei" {{ $zis->bulan == 'Mei' ? 'selected' : '' }}>Mei</option>
                                        <option value="Juni" {{ $zis->bulan == 'Juni' ? 'selected' : '' }}>Juni</option>
                                        <option value="Juli" {{ $zis->bulan == 'Juli' ? 'selected' : '' }}>Juli</option>
                                        <option value="Agustus" {{ $zis->bulan == 'Agustus' ? 'selected' : '' }}>Agustus</option>
                                        <option value="September" {{ $zis->bulan == 'September' ? 'selected' : '' }}>September</option>
                                        <option value="Oktober" {{ $zis->bulan == 'Oktober' ? 'selected' : '' }}>Mei</option>
                                        <option value="November" {{ $zis->bulan == 'November' ? 'selected' : '' }}>November</option>
                                        <option value="Desember" {{ $zis->bulan == 'Desember' ? 'selected' : '' }}>Desember</option>
                                    </select>
                                    @error('bulan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-lg-6">
                                    <label for="tahun">Tahun</label>
                                    <select class="selectpicker form-select @error('tahun') is-invalid @enderror tahun-select"
                                        name="tahun" data-live-search="true" data-size="6" data-width="100%"
                                        title="-- Silahkan Pilih --" data-style="border border-gray rounded" required>
                                        <option>-- Silahkan Pilih --</option>
                                        <?php
                                            $thn_skr = date('Y');
                                            for ($x = $thn_skr; $x >= 2015; $x--) {
                                            ?>
                                        <option value="<?php echo $x; ?>" {{ $x == $zis->tahun ? 'selected' : '' }}>
                                            <?php echo $x; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    @error('tahun')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="zakat">Zakat</label>
                                <input type="text"
                                    class="form-control form-control-sm @error('zakat') is-invalid @enderror" id="zakat"
                                    name="zakat" placeholder="Masukkan nominal tanpa titik/koma (cont: 1000000)"
                                    value="{{ $zis->zakat, old('zakat') }}" required>
                                @error('zakat')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="infaq_terikat">Infaq Terikat</label>
                                <input type="text"
                                    class="form-control form-control-sm @error('infaq_terikat') is-invalid @enderror"
                                    id="infaq_terikat" name="infaq_terikat"
                                    placeholder="Masukkan nominal tanpa titik/koma (cont: 1000000)"
                                    value="{{ $zis->infaq_terikat, old('infaq_terikat') }}" required>
                                @error('infaq_terikat')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="infaq_umum">Infaq Umum</label>
                                <input type="text"
                                    class="form-control form-control-sm @error('infaq_umum') is-invalid @enderror"
                                    id="infaq_umum" name="infaq_umum"
                                    placeholder="Masukkan nominal tanpa titik/koma (cont: 1000000)"
                                    value="{{ $zis->infaq_umum, old('infaq_umum') }}" required>
                                @error('infaq_umum')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <input type="hidden" class="" id="" name="user"
                                value="{{ Auth::user()->id }}">

                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button type="reset" class="btn btn-inverse-danger btn-fw">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
