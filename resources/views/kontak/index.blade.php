@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title">Kontak</h4>
                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahkontak">
                                + Tambah
                            </button>
                        </div>
                        <div class="d-flex justify-content-between mt-3">
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label class="px-2 mt-2">Cari:</label>
                                    <input type="text" class="form-control form-control-sm" id="searchInput" placeholder="Cari di sini...">
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive mb-3">
                            <table class="table table-striped" id="myTable" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama Kontak</th>
                                        <th class="text-center">Kontak</th>
                                        <th class="text-center" class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($kontak as $kon)
                                    <tr>
                                        <td class="text-center">{{ $loop->iteration }}</td>
                                        <td class="text-center">{{ $kon->nama_kontak }}</td>
                                        <td class="text-center">{{ $kon->kontak }}</td>
                                        <td class="d-flex justify-content-center">
                                            <button class="btn btn-inverse-info btn-icon mx-1" data-toggle="modal" data-target="#updatekontak{{ $kon->id }}"><i class="ti-pencil"></i></button>
                                            <form class="btn-delete" action="/pengaturan/kontak/delete/{{ $kon->id }}" method="post">
                                                @method('delete')
                                                @csrf
                                                <button type="submit" class="btn btn-inverse-danger btn-icon">
                                                    <i class="ti-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal Tambah Kontak -->
    <div class="modal fade" id="tambahkontak" role="dialog" aria-labelledby="tambahkontakTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="tambahkontakTitle" style="padding-left: 32%" ;>
                        Tambah Kontak</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('pengaturan/kontak/store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama_kontak">Nama Kontak</label>
                            <select class="form-control  @error('nama_kontak') is-invalid @enderror" width="100%" name="nama_kontak" id="nama_kontak" required>
                                <option selected>-- Silahkan Pilih --</option>
                                <option value="Telepon">Telepon</option>
                                <option value="Alamat">Alamat</option>
                                <option value="Email">Email</option>
                            </select>
                            @error('nama_kontak')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="kontak">Kontak</label>
                            <input type="text"
                                class="form-control form-control-sm @error('kontak') is-invalid @enderror" id="kontak"
                                name="kontak"
                                value="{{ old('kontak') }}" placeholder="masukkan kontak" required>
                            @error('kontak')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Edit Sosmed -->
    @foreach ($kontak as $kon)
        <div class="modal fade" id="updatekontak{{ $kon->id }}" role="dialog" aria-labelledby="updatekontakTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 align="center" class="modal-title" id="updatekontakTitle" style="padding-left: 32%" ;>
                            Update Kontak</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('/pengaturan/kontak/update/' . $kon->id) }}" method="POST">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label for="nama_kontak">Nama Kontak</label>
                                <input type="text"value="{{ $kon->nama_kontak }}" class="form-control form-control-sm" readonly>
                            </div>
                            <div class="form-group">
                                <label for="kontak">Kontak</label>
                                <input type="text"
                                    class="form-control form-control-sm @error('kontak') is-invalid @enderror" id="kontak"
                                    name="kontak"
                                    value="{{ $kon->kontak, old('kontak') }}" placeholder="url sosial media" required>
                                @error('kontak')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection