@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title">Sosial Media</h4>
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambahsosmed">
                                    + Tambah
                                </button>
                        </div>
                        <div class="d-flex justify-content-between mt-3">
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label class="px-2 mt-2">Cari:</label>
                                    <input type="text" class="form-control form-control-sm" id="searchInput" placeholder="Cari di sini...">
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive mb-3">
                            <table class="table table-striped" id="myTable" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama Sosial Media</th>
                                        <th class="text-center">Link Sosial Media</th>
                                        <th class="text-center" class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sosmed as $sos)
                                    <tr>
                                        <td class="text-center">{{ $loop->iteration }}</td>
                                        <td class="text-center">{{ $sos->nama_sosmed }}</td>
                                        <td class="text-center">{{ $sos->link }}</td>
                                        <td class="d-flex justify-content-center">
                                            <button class="btn btn-inverse-info btn-icon mx-1" data-toggle="modal" data-target="#updatesosmed{{ $sos->id }}"><i class="ti-pencil"></i></button>
                                            <form class="btn-delete" action="/pengaturan/sosmed/delete/{{ $sos->id }}" method="post">
                                                @method('delete')
                                                @csrf
                                                <button type="submit" class="btn btn-inverse-danger btn-icon">
                                                    <i class="ti-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Tambah Sosmed -->
    <div class="modal fade" id="tambahsosmed" role="dialog" aria-labelledby="tambahsosmedTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="tambahsosmedTitle" style="padding-left: 32%" ;>
                        Tambah Sosial Media</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('pengaturan/sosmed/store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama_sosmed">Nama Sosial Media</label>
                            <select class="form-control  @error('nama_sosmed') is-invalid @enderror" width="100%" name="nama_sosmed" id="nama_sosmed" required>
                                <option selected>-- Silahkan Pilih --</option>
                                <option value="Whatsapp">Whatsapp</option>
                                <option value="Twitter">Twitter</option>
                                <option value="Instagram">Instagram</option>
                                <option value="Facebook">Facebook</option>
                                <option value="Youtube">Youtube</option>
                            </select>
                            @error('nama_sosmed')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="link">URL Link Sosial Media</label>
                            <input type="url"
                                class="form-control form-control-sm @error('link') is-invalid @enderror" id="link"
                                name="link"
                                value="{{ old('link') }}" placeholder="url sosial media" required>
                            @error('link')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Edit Sosmed -->
    @foreach ($sosmed as $sos)
        <div class="modal fade" id="updatesosmed{{ $sos->id }}" role="dialog" aria-labelledby="updatesosmedTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 align="center" class="modal-title" id="updatesosmedTitle" style="padding-left: 32%" ;>
                            Update Sosial Media</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('/pengaturan/sosmed/update/' . $sos->id) }}" method="POST">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label for="nama_sosmed">Nama Sosial Media</label>
                                <select class="form-control  @error('nama_sosmed') is-invalid @enderror" width="100%" name="nama_sosmed" id="nama_sosmed" required>
                                    <option value="Whatsapp" {{ $sos->nama_sosmed == 'Whatsapp' ? 'selected' : '' }}>Whatsapp</option>
                                    <option value="Twitter" {{ $sos->nama_sosmed == 'Twitter' ? 'selected' : '' }}>Twitter</option>
                                    <option value="Instagram" {{ $sos->nama_sosmed == 'Instagram' ? 'selected' : '' }}>Instagram</option>
                                    <option value="Facebook" {{ $sos->nama_sosmed == 'Facebook' ? 'selected' : '' }}>Facebook</option>
                                    <option value="Youtube" {{ $sos->nama_sosmed == 'Youtube' ? 'selected' : '' }}>Youtube</option>
                                </select>
                                @error('nama_sosmed')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="link">URL Link Sosial Media</label>
                                <input type="url"
                                    class="form-control form-control-sm @error('link') is-invalid @enderror" id="link"
                                    name="link"
                                    value="{{ $sos->link, old('link') }}" placeholder="url sosial media" required>
                                @error('link')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection