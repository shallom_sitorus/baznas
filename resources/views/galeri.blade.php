@extends('layouts.landingpage')

@section('content')
    <section class="news-detail-header-section text-center">
        <div class="section-overlay"></div>

        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-12">
                    <h1 class="text-white">Artikel dan Berita</h1>
                </div>

            </div>
        </div>
    </section>

    <section class="news-section section-padding">
        <div class="container">
            <div class="row">
                @foreach ($galeri as $album)
                    <div class="col-md-4">
                        <a data-bs-toggle="modal" data-bs-target="#modalGaleri{{ $album->id }}">
                            <div class="card custom-block-wrap" style="width: 18rem;">
                                @foreach ($album->image->take(1) as $gambar)
                                    <img src="{{ asset('admin/images/galeri/' . $gambar->foto) }}" class="card-img-top"
                                        alt="Image 2">
                                @endforeach
                                <div class="card-body">
                                    <h5 class="card-title">{{ $album->judul }}</h5>
                                    <p class="card-text">{{ $album->deskripsi }}</p>
                                </div>
                            </div>
                        </a>

                    </div>
                @endforeach

            </div>
        </div>
    </section>

    <!-- Modal -->
    @foreach ($galeri as $album)
        @foreach ($album->image->take(1) as $gambar)
            <div class="modal fade" id="modalGaleri{{ $album->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                                <div class="carousel-indicators">
                                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0"
                                        class="active" aria-current="true" aria-label="Slide 1"></button>
                                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                                        aria-label="Slide 2"></button>
                                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                                        aria-label="Slide 3"></button>
                                </div>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="{{ asset('admin/images/galeri/' . $gambar->foto) }}" class="d-block w-100"
                                            alt="...">
                                        {{-- <div class="carousel-caption bg-dark d-none d-md-block">
                                            <h5>{{ $album->judul }}</h5>
                                            <p>{{ $album->deskripsi }}</p>
                                        </div> --}}
                                    </div>
                                    @foreach ($album->image->skip(1) as $g)
                                        <div class="carousel-item ">
                                            <img src="{{ asset('admin/images/galeri/' . $g->foto) }}" class="d-block w-100"
                                                alt="...">
                                        </div>
                                    @endforeach
                                </div>
                                <button class="carousel-control-prev" type="button"
                                    data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button"
                                    data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endforeach
@endsection
