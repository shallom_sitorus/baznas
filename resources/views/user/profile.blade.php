@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="ml-xl-4 mt-3">
                            <div class="d-flex justify-content-between">
                                <p class="card-title">User Profile</p>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="/profile/edit/{{ $users->id }}" type="button" class="btn btn-inverse-secondary btn-fw"><i class="ti-pencil-alt"></i></a>
                                  </div>
                            </div>
                                <div class="d-flex justify-content-center py-4">
                                    @if ($users->foto)
                                        <img src="{!! asset('admin/images/user/' . $users->foto) !!}" class="img-preview round-image" style="height: 200px" alt="...">
                                    @else
                                        <img src="{!! asset('admin/images/user/defaultFoto.png') !!}" class="img-preview round-image" style="height: 200px" alt="...">
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="name">Nama User</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                            class="form-control form-control-sm @error('name') is-invalid @enderror"
                                            id="name" name="name" placeholder="Nama User"
                                            value="{{ $users->name }}" readonly>
                                        @error('name')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="email">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                            class="form-control form-control-sm @error('email') is-invalid @enderror"
                                            id="email" name="email" placeholder="Email"
                                            value="{{ $users->email }}" readonly>
                                        @error('email')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

