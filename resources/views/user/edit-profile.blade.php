@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="ml-xl-4 mt-3">
                            <div class="d-flex justify-content-between">
                                <p class="card-title">Edit User Profile</p>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="/profile" type="button" class="btn btn-inverse-secondary btn-fw"><i
                                            class="ti-arrow-left"></i></a>
                                </div>
                            </div>

                            <form action="/profile/update/{{ $users->id }}" method="post" class="forms-sample"
                                enctype="multipart/form-data">
                                @method('put')
                                @csrf
                                <div class="form-group d-flex justify-content-center pb-5">
                                    <input type="hidden" name="oldImage" value="{{ $users->foto }}">
                                    <div class="custom-file col-md-3 mb-5">
                                        @if ($users->foto != null)
                                            <img src="{!! asset('admin/images/user/' . $users->foto) !!}" class="img-preview round-image col-sm-12"
                                                alt="...">
                                        @else
                                            <img src="{!! asset('admin/images/user/defaultFoto.png') !!}" class="img-preview round-image col-sm-12"
                                                alt="...">
                                        @endif
                                        <input type="file" class="custom-file-input @error('foto') is-invalid @enderror"
                                            name="foto" id="image" onchange="previewImage()">
                                        <label for="image" class="file-upload">
                                            <i class="fa fa-pencil"></i>
                                        </label>
                                    </div>
                                    @error('foto')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group row pt-5">
                                    <label class="col-sm-2 mt-3" for="name">Nama User</label>
                                    <div class="col-sm-10 mt-3">
                                        <input type="text"
                                            class="form-control form-control-sm @error('name') is-invalid @enderror"
                                            id="name" name="name" placeholder="Nama User"
                                            value="{{ $users->name }}">
                                        @error('name')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="email">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email"
                                            class="form-control form-control-sm @error('email') is-invalid @enderror"
                                            id="email" name="email" placeholder="Email" value="{{ $users->email }}">
                                        @error('email')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="password">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password"
                                            class="form-control form-control-sm @error('password') is-invalid @enderror"
                                            id="password" name="password" placeholder="password"
                                            value="{{ $users->password }}">
                                        @error('password')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <input type="hidden" id="password_lama" name="password_lama"
                                    value="{{ $users->password }}">
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button type="reset" class="btn btn-inverse-danger btn-fw">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
