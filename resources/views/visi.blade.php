@extends('layouts.landingpage')

@section('content')

<section class="news-detail-header-section text-center">
    <div class="section-overlay"></div>

    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-12">
                <h1 class="text-white">Visi & Misi</h1>
            </div>

        </div>
    </div>
</section>

<section class="section-padding section-bg">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-12 mb-5 mb-lg-0">
                <img src="{!! asset('admin/images/tentang/'. $tentang[0]->gambarvimi) !!}" class="custom-text-box-image img-fluid" alt="">
            </div>

            <div class="col-lg-6 col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="custom-text-box mb-lg-0">
                            <h5 class="mb-3">Visi</h5>

                            <p>{!! $tentang[0]->visi !!}</p>

                            <h5 class="mb-3">Misi</h5>

                            <p>{!! $tentang[0]->misi !!}</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection