@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title">Artikel/Berita</h4>
                                <a href="/artikel/create"><button type="button" class="btn btn-success btn-sm">
                                    + Tambah
                                </button></a>
                        </div>
                        <div class="d-flex justify-content-between mt-3">
                            <form action="{{ url('/artikel') }}" method="GET">
                                @csrf
                                <div class="form-group d-flex">
                                    <label class="px-2 mt-2">Tahun:</label>
                                    <select class="form-control form-control-sm" name="tahun">
                                        @foreach ($tahun as $thn)
                                            <option value="{{ $thn }}" @if (request()->tahun)
                                                {{ request()->tahun == $thn ? 'selected' : '' }}
                                            @else
                                                {{ date('Y') == $thn ? 'selected' : '' }}
                                            @endif>{{ $thn }}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit" class="btn btn-sm btn-secondary text-white"
                                        style="margin-left: 5px;">Cari</button>
                                </div>
                            </form>
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label class="px-2 mt-2">Cari:</label>
                                    <input type="text" class="form-control form-control-sm" id="searchInput" placeholder="Cari di sini...">
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive mb-3">
                            <table class="table table-striped" id="myTable" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th width="5%" class="text-center">No</th>
                                        <th width="30%">Judul</th>
                                        <th width="30%">Isi</th>
                                        <th width="15%">Penulis</th>
                                        <th width="20%" class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($artikel as $art)
                                        <tr>
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            <td>{{ Str::words($art->judul, 5, '...') }}</td>
                                            <td>{{ Str::words(strip_tags($art->body), 6, '...') }}</td>
                                            <td class="text-center">{{ $art->author->name }}</td>
                                            <td class="d-flex justify-content-center">
                                                <a href="/artikel/detail/{{ $art->slug }}"><button class="btn btn-inverse-primary btn-icon"><i class="ti-eye"></i></button></a>
                                                <a href="/artikel/edit/{{ $art->slug }}"><button class="btn btn-inverse-info btn-icon mx-1"><i class="ti-pencil"></i></button></a>
                                                <form class="btn-delete" action="/artikel/delete/{{ $art->slug }}" method="post">
                                                    @method('delete')
                                                    @csrf
                                                    <button type="submit" class="btn btn-inverse-danger btn-icon">
                                                        <i class="ti-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
