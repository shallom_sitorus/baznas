@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <a href="/artikel">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#6c757d"
                                    class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                                </svg>
                            </a>
                            <p class="card-description mx-2">Kembali</p>
                        </div>
                        <h4 class="card-title text-center">Form Edit Artikel/Berita</h4>
                        <form action="{{ url('/artikel/update/' . $artikel->slug) }}" method="post" class="forms-sample" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <input type="hidden" name="user_id" value="{{ $artikel->user_id }}">
                            <div class="form-group">
                                <label for="judul">Judul</label>
                                <input type="text"
                                    class="form-control form-control-sm @error('judul') is-invalid @enderror" id="judul"
                                    name="judul" placeholder="Judul" value="{{ old('judul', $artikel->judul) }}">
                                @error('judul')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <input type="text"
                                    class="form-control form-control-sm @error('slug') is-invalid @enderror" id="slug"
                                    name="slug" placeholder="Slug" value="{{ old('slug', $artikel->slug) }}">
                                @error('slug')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Upload Gambar</label>
                                <input type="hidden" name="oldImage" value="{{ $artikel->image }}">
                                @if ($artikel->image)
                                    <img src="{{ asset('admin/images/artikel/' . $artikel->image) }}"
                                        class="img-preview img-fluid mb-3 col-sm-5 d-block">
                                @else
                                    <img class="img-preview img-fluid mb-3 col-sm-5">
                                @endif
                                <img class="img-preview img-fluid mb-3 col-sm-5">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input @error('image') is-invalid @enderror"
                                        name="image" id="image" onchange="previewImage()">
                                    <label class="custom-file-label text-secondary"
                                        for="image">{{ $artikel->image ?? 'Choose file' }}</label>
                                </div>
                                @error('image')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="body">Isi Artikel</label>
                                <input id="body" type="hidden" name="body"
                                    value="{{ old('body', $artikel->body) }}">
                                <trix-editor input="body"></trix-editor>
                                @error('body')
                                    <small>
                                        <p class="text-danger">{{ $message }}</p>
                                    </small>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button type="reset" class="btn btn-inverse-danger btn-fw">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
