@extends('layouts.landingpage')

@section('content')
    <section class="section-padding section-bg" id="">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12 mb-5 mb-lg-0">
                    <div class="custom-text-box mb-lg-0">
                        <h3 class="mb-4 text-center" style="color: #3f9b88">Kalkulator Zakat Maal</h3>

                        <p class="mt-3" align="justify">Kalkulator zakat adalah layanan
                            untuk mempermudah perhitungan jumlah
                            zakat yang harus ditunaikan oleh setiap
                            umat muslim sesuai ketetapan syariah.
                            Oleh karena itu, bagi Anda yang ingin
                            mengetahui berapa jumlah zakat yang harus
                            ditunaikan, silahkan gunakan fasilitas Kalkulator
                            Zakat BAZNAS dibawah ini.
                        </p>

                        <p align="justify">Zakat maal yang dimaksud dalam perhitungan ini adalah zakat yang dikenakan atas uang, emas, surat berharga, dan aset yang disewakan. Tidak termasuk harta pertanian, pertambangan, dan lain-lain yang diatur dalam UU No.23/2011 tentang pengelolaan zakat. Zakat maal harus sudah mencapai nishab (batas minimum) dan terbebas dari hutang serta kepemilikan telah mencapai 1 tahun (haul). Nishab zakat maal sebesar 85 gram emas. Kadar zakatnya senilai 2,5%. (Sumber: Al Qur'an Surah Al Baqarah ayat 267, Peraturan Menteri Agama Nomer 31 Tahun 2019, Fatwa MUI Nomer 3 Tahun 2003, dan pendapat Shaikh Yusuf Qardawi). 
                        </p>
                        <p align="justify">Standar harga emas yg digunakan untuk 1 gram nya adalah Rp{{ number_format($emas[0]->harga, 0, ',', '.') }},-. Sementara nishab yang digunakan adalah sebesar 85 gram emas.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="custom-text-box">
                        <form>
                            @csrf
                            <input type="hidden" name="hargaemas" id="hargaemas" value="{{ $emas[0]->harga }}">
                            <div class="mb-4">
                                <p align="left"><b>Nilai emas, perak, dan/atau permata</b></p>
                                <input class="form-control" name="perhiasan" id="perhiasan" type="text"
                                    placeholder="Masukkan nominal tanpa titik/koma">
                            </div>
                            <div class="mb-4">
                                <p align="left"><b>Uang tunai, tabungan, deposito, tunjangan</b></p>
                                <input class="form-control" name="tabungan" id="tabungan" type="text"
                                    placeholder="Masukkan nominal tanpa titik/koma">
                            </div>
                            <div>
                                <p align="left"><b>Hutang/cicilan</b></p>
                                <input class="form-control" name="hutang" id="hutang" type="text"
                                    placeholder="Masukkan nominal tanpa titik/koma">
                            </div>
                            <br>
                            <div>
                                <button type="button" onclick="zakatMaal()" id="hitung-zakat"
                                    class="btn btn-success">Hitung
                                    Zakat</button>
                            </div>
                        </form>
                    </div>

                    <div class="row" id="card"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
