@extends('layouts.landingpage')

@section('content')
    <section class="section-padding section-bg" id="">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12 mb-5 mb-lg-0">
                    <div class="custom-text-box mb-lg-0">
                        <h3 class="mb-4 text-center" style="color: #3f9b88">Kalkulator Zakat Penghasilan</h3>

                        <p class="mt-3" align="justify">Kalkulator zakat adalah layanan
                            untuk mempermudah perhitungan jumlah
                            zakat yang harus ditunaikan oleh setiap
                            umat muslim sesuai ketetapan syariah.
                            Oleh karena itu, bagi Anda yang ingin
                            mengetahui berapa jumlah zakat yang harus
                            ditunaikan, silahkan gunakan fasilitas Kalkulator
                            Zakat BAZNAS dibawah ini.
                        </p>

                        <p align="justify">Zakat penghasilan atau yang dikenal juga sebagai zakat
                            profesi adalah bagian dari zakat maal yang wajib dikeluarkan
                            atas harta yang berasal dari pendapatan / penghasilan rutin dari
                            pekerjaan yang tidak melanggar syariah. Nishab zakat penghasilan
                            sebesar 85 gram emas per tahun. Kadar zakat penghasilan senilai 2,5%.
                            Dalam praktiknya, zakat penghasilan dapat ditunaikan setiap bulan dengan
                            nilai nishab per bulannya adalah setara dengan nilai seperduabelas dari
                            85 gram emas, dengan kadar 2,5%. Jadi apabila penghasilan setiap bulan
                            telah melebihi nilai nishab bulanan, maka wajib dikeluarkan zakatnya sebesar
                            2,5% dari penghasilannya tersebut.
                            (Sumber: Al Qur'an Surah Al Baqarah ayat 267, Peraturan Menteri Agama Nomer
                            31 Tahun 2019, Fatwa MUI Nomer 3 Tahun 2003, dan pendapat Shaikh Yusuf Qardawi).
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="custom-text-box">
                        <form>
                            @csrf
                            <input type="hidden" name="hargaemas" id="hargaemas" value="{{ $emas[0]->harga }}">
                            <div class="mb-4">
                                <p align="left"><b>Jumlah Pendapatan Per-Bulan</b></p>
                                <input class="form-control" name="pendapatan" id="pendapatan" type="text"
                                    placeholder="Masukkan nominal tanpa titik/koma">
                            </div>
                            <div>
                                <p align="left"><b>Bonus, THR dan lainnya</b></p>
                                <input class="form-control" name="bonus" id="bonus" type="text"
                                    placeholder="Masukkan nominal tanpa titik/koma">
                            </div>
                            <br>
                            <div>
                                <button type="button" onclick="zakatPenghasilan()" id="hitung-zakat"
                                    class="btn btn-success">Hitung
                                    Zakat</button>
                            </div>
                        </form>
                    </div>

                    <div class="row" id="card">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
