@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title">Rekening</h4>
                                <a href="/pengaturan/rekening/create"><button type="button" class="btn btn-success btn-sm">
                                    + Tambah
                                </button></a>
                        </div>
                        <div class="d-flex justify-content-between mt-3">
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label class="px-2 mt-2">Cari:</label>
                                    <input type="text" class="form-control form-control-sm" id="searchInput" placeholder="Cari di sini...">
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive mb-3">
                            <table class="table table-striped" id="myTable" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama Bank</th>
                                        <th class="text-center">No. Rekening</th>
                                        <th class="text-center">Nama Pemilik Rekening</th>
                                        <th class="text-center">Logo</th>
                                        <th class="text-center" class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($rekening as $rek)
                                    <tr>
                                        <td class="text-center">{{ $loop->iteration }}</td>
                                        <td class="text-center">{{ $rek->bank }}</td>
                                        <td class="text-center">{{ $rek->no_rekening }}</td>
                                        <td class="text-center">{{ $rek->nama_rekening }}</td>
                                        <td class="text-center"> 
                                            <img src="{{ asset('admin/images/rekening/' . $rek->logo) }}" class="img-thumbnail" alt="gambar-logo-bank">
                                        </td>
                                        <td class="d-flex justify-content-center">
                                            <a href="/pengaturan/rekening/edit/{{ $rek->id }}"><button class="btn btn-inverse-info btn-icon mx-1"><i class="ti-pencil"></i></button></a>
                                            <form class="btn-delete" action="/pengaturan/rekening/delete/{{ $rek->id }}" method="post">
                                                @method('delete')
                                                @csrf
                                                <button type="submit" class="btn btn-inverse-danger btn-icon">
                                                    <i class="ti-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection