function zakatPenghasilan() {
    const penghasilan = parseInt(document.getElementById("pendapatan").value) || 0;
    const bonus = parseInt(document.getElementById("bonus").value) || 0;

    const hargaEmas = parseInt(document.getElementById("hargaemas").value);
    const nisab = hargaEmas * 85 / 12;
    const totalHarta = penghasilan + bonus;
    const zakat = totalHarta >= nisab ? totalHarta * 0.025 : 0;

    var hasil = zakat.toLocaleString('id-ID', {
        style: 'currency',
        currency: 'IDR'
    });
    var penghasilanString = penghasilan.toLocaleString('id-ID', {
        style: 'currency',
        currency: 'IDR'
    });
    var bonusString = bonus.toLocaleString('id-ID', {
        style: 'currency',
        currency: 'IDR'
    });

    var htmlHasil = '<div class="col-lg-12 col-md-12 col-12">' +
        '<div class="custom-text-box mb-lg-0">' +
        '<p class="text-center">' + '<b>Jumlah Zakat Penghasilan Anda</b>' + '</p>' +
        '<h3 class="mb-3 text-center">' + hasil + '</h3>';
    if (totalHarta < nisab) {
        htmlHasil += '<p class="text-center" style="color: #3f9b88">' +
            '<b>Penghasilan Anda belum mencapai nishab</b>' +
            '</p>';
    }

    htmlHasil += '<table class="table table-borderless text-secondary">' +
        '<tr>' +
        '<td>Pendapatan per bulan:</td>' +
        '<td>' + penghasilanString + '</td>' +
        '</tr>' +
        '<tr>' +
        ' <td>Bonus, THR, dan lainnya:</td>' +
        '<td>' + bonusString + '</td>' +
        '</tr>' +
        '</table>' +
        '</div>' +
        '</div>';

    var cardContainer = document.getElementById("card");
    cardContainer.innerHTML = htmlHasil;
}

function zakatMaal() {
    const perhiasan = parseInt(document.getElementById("perhiasan").value) || 0;
    const tabungan = parseInt(document.getElementById("tabungan").value) || 0;
    const hutang = parseInt(document.getElementById("hutang").value) || 0;

    const hargaEmas = parseInt(document.getElementById("hargaemas").value);
    const nisab = hargaEmas * 85;
    const totalHarta = perhiasan + tabungan - hutang;
    const zakat = totalHarta >= nisab ? totalHarta * 0.025 : 0;

    var hasil = zakat.toLocaleString('id-ID', {
        style: 'currency',
        currency: 'IDR'
    });
    var perhiasanString = perhiasan.toLocaleString('id-ID', {
        style: 'currency',
        currency: 'IDR'
    });
    var tabunganString = tabungan.toLocaleString('id-ID', {
        style: 'currency',
        currency: 'IDR'
    });
    var hutangString = hutang.toLocaleString('id-ID', {
        style: 'currency',
        currency: 'IDR'
    });

    var htmlHasil = '<div class="col-lg-12 col-md-12 col-12">' +
        '<div class="custom-text-box mb-lg-0">' +
        '<p class="text-center">' + '<b>Jumlah Zakat Maal Anda</b>' + '</p>' +
        '<h3 class="mb-3 text-center">' + hasil + '</h3>';
    if (totalHarta < nisab) {
        htmlHasil += '<p class="text-center" style="color: #3f9b88">' +
            '<b>Harta Anda belum mencapai nishab</b>' +
            '</p>';
    }

    htmlHasil += '<table class="table table-borderless text-secondary">' +
        '<tr>' +
        '<td>Emas, perak, permata:</td>' +
        '<td>' + perhiasanString + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Tabungan dan lainnya:</td>' +
        '<td>' + tabunganString + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Hutang dan cicilan:</td>' +
        '<td>' + hutangString + '</td>' +
        '</tr>' +
        '</table>' +
        '</div>' +
        '</div>';

    var cardContainer = document.getElementById("card");
    cardContainer.innerHTML = htmlHasil;
}