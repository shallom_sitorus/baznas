function editForm() {
    let inputs = document.querySelectorAll('input[type="text"], textarea, input[type="date"]');
    inputs.forEach((input) => {
        input.readOnly = false;
    });
    document.getElementById("hidden-element").style.display = "block";
    document.getElementById("simpan-btn").style.display = "block";
    document.getElementById("batal-btn").style.display = "block";
    document.getElementById("edit-btn").style.display = "none";
}

function cancelEdit() {
    let inputs = document.querySelectorAll('input[type="text"], textarea, input[type="date"]');
    inputs.forEach((input) => {
        input.readOnly = true;
    });
    document.getElementById("hidden-element").style.display = "none";
    document.getElementById("simpan-btn").style.display = "none";
    document.getElementById("batal-btn").style.display = "none";
    document.getElementById("edit-btn").style.display = "block";
}