<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GaleriController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PengaturanController;
use App\Http\Controllers\PenerimazisController;
// use App\Http\Controllers\KalkulatorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome', [
//         'artikel' => Artikel::latest()->get()
//     ]);
// });

Auth::routes();

Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Route::middleware(['auth'])->controller(DashboardController::class)->group(function () {
    Route::get('/dashboard', 'index');
});

Route::middleware(['auth'])->controller(UserController::class)->group(function () {
    Route::get('/profile', 'index');
    Route::get('/profile/edit/{id}', 'edit');
    Route::put('/profile/update/{id}', 'update');
});

Route::middleware(['auth'])->controller(ArtikelController::class)->group(function () {
    Route::get('/artikel', 'index');
    Route::get('/artikel/create', 'create');
    Route::post('/artikel/store', 'store');
    Route::get('/artikel/edit/{artikel:slug}', 'edit');
    Route::put('/artikel/update/{artikel:slug}', 'update');
    Route::get('/artikel/delete/{artikel:slug}', 'delete');
    Route::get('/artikel/detail/{artikel:slug}', 'show');
    Route::get('/artikel/checkSlug', 'checkSlug');
    Route::post('attachments', 'attachStore')->name('attachments.store');
});

Route::middleware(['auth'])->controller(GaleriController::class)->group(function () {
    Route::get('/galeri', 'index');
    Route::get('/galeri/create', 'create');
    Route::post('/galeri/store', 'store');
    Route::get('/galeri/detail/{id}', 'show');
    Route::get('/galeri/edit/{id}', 'edit');
    Route::put('/galeri/update/{id}', 'update');
    Route::get('/galeri/delete/{id}', 'delete');
    // Route::put('/artikel/update/{artikel:slug}', 'update');
    // Route::get('/artikel/detail/{artikel:slug}', 'show');
    // Route::get('/artikel/checkSlug', 'checkSlug');
    // Route::post('attachments', 'attachStore')->name('attachments.store');
});

Route::middleware(['auth'])->controller(PenerimazisController::class)->group(function () {
    Route::get('/laporan_zis', 'index');
    Route::get('/laporan_zis/create', 'create');
    Route::post('/laporan_zis/store', 'store');
    Route::get('/laporan_zis/edit/{id}', 'edit');
    Route::put('/laporan_zis/update/{id}', 'update');
    Route::get('/laporan_zis/detail/{id}', 'show');
    Route::get('/laporan_zis/delete/{id}', 'delete');
});

// Route::middleware(['auth'])->controller(KalkulatorController::class)->group(function () {
//     Route::get('/pengaturan/kalkulator', 'hargaEmas');
//     Route::put('/pengaturan/kalkulator/hargaemas/{id}', 'updateHargaEmas');
// });

Route::middleware(['auth'])->controller(PengaturanController::class)->group(function () {
    Route::get('/pengaturan/tentangkami', 'tentangkami');
    Route::get('/pengaturan/edit-profil/{id}', 'editProfil');
    Route::put('/pengaturan/update-profil/{id}', 'updateProfil');
    Route::get('/pengaturan/edit-vimi/{id}', 'editVimi');
    Route::put('/pengaturan/update-vimi/{id}', 'updateVimi');
    Route::get('/pengaturan/kalkulator', 'hargaEmas');
    Route::put('/pengaturan/kalkulator/hargaemas/{id}', 'updateHargaEmas');
    Route::get('/pengaturan/rekening', 'rekening');
    Route::get('/pengaturan/rekening/create', 'createRekening');
    Route::post('/pengaturan/rekening/store', 'storeRekening');
    Route::get('/pengaturan/rekening/edit/{id}', 'editRekening');
    Route::put('/pengaturan/rekening/update/{id}', 'updateRekening');
    Route::get('/pengaturan/rekening/delete/{id}', 'deleteRekening');
    Route::get('/pengaturan/sosmed', 'sosmed');
    Route::post('/pengaturan/sosmed/store', 'storeSosmed');
    Route::put('/pengaturan/sosmed/update/{id}', 'updateSosmed');
    Route::get('/pengaturan/sosmed/delete/{id}', 'deleteSosmed');
    Route::get('/pengaturan/kontak', 'kontak');
    Route::post('/pengaturan/kontak/store', 'storeKontak');
    Route::put('/pengaturan/kontak/update/{id}', 'updateKontak');
    Route::get('/pengaturan/kontak/delete/{id}', 'deleteKontak');
});

Route::get('/', [LandingController::class, 'index'])->name('home');
Route::get('/posts', [LandingController::class, 'posts'])->name('allposts');
Route::get('/landing-galeri', [LandingController::class, 'galeri']);
Route::get('/{artikel:slug}', [LandingController::class, 'detailPost'])->name('detailpost');
Route::get('/tentang_kami/profil', [LandingController::class, 'tentang']);
Route::get('/tentang_kami/visi', [LandingController::class, 'visi']);
Route::get('/kalkulator/zakat_penghasilan', [LandingController::class, 'penghasilan']);
Route::post('/hitung_penghasilan', [LandingController::class, 'HitungPenghasilan']);
Route::get('/kalkulator/zakat_maal', [LandingController::class, 'maal']);
Route::get('/penerima_zis/zis', [LandingController::class, 'zis']);
Route::get('/rekening/baznas', [LandingController::class, 'rekening']);
