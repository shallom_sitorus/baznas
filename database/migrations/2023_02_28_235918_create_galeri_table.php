<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeri', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->text('deskripsi');
            $table->date('tanggal');
            $table->timestamps();
        });

        Schema::create('galeri_image', function (Blueprint $table) {
            $table->id();
            $table->foreignId('galeri_id')->nullable()->constrained('galeri')->cascadeOnUpdate()->nullOnDelete();
            $table->string('foto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galeri_image');
        Schema::dropIfExists('galeri');
    }
};
