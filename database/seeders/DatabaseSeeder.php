<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Tentang;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            ArtikelSeeder::class,
            PenerimazisSeeder::class,
            EmasSeeder::class,
            GaleriSeeder::class,
            TentangSeeder::class,
            KontakSeeder::class,
        ]);
    }
}
