<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ArtikelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artikel')->insert([[
            'judul' => 'Baznas Sukoharjo Gelar Khitanan Massal',
            'slug' => 'baznas-sukoharjo-gelar-khitanan-massal',
            'image' => '20230218015334_IMG-20220702-WA0036.jpg',
            'body' => '<div><strong>SUKOHARJO – Badan Amil Zakat Nasional (Baznas) Sukoharjo</strong> menggelar khitan massal di masa libur sekolah, Sabtu (2/7/2022) di Dinas Kesehatan Kabupaten (DKK). Khitanan massal tersebut diikuti 250 anak yang berasal dari keluarga kurang mampu di Kabupaten Sukoharjo.<br><br></div><div>Ketua Baznas Sukoharjo Sardiyono mengatakan, setiap anak mendapat paket berupa sarung, baju koko, kopiah, tas, paket alat tulis, dan uang saku Rp150 ribu serta suvenir dari Bank Jateng.<br><br></div><div>“Dalam khitan massal ini ada 20 tim petugas khitan, di mana satu tim terdiri dari satu dokter dan satu paramedis yang berasal dari Puskesmas, rumah sakit daerah, dan rumah sakit swasta se-Sukoharjo,” kata Sardiyono.<br><br></div><div>Sementara itu, dalam sambutannya Bupati Sukoharjo Etik Suryani menyampaikan, khitan adalah salah satu bagian dari syariat agama dan wajib hukumnya bagi anak laki-laki. Dari pandangan agama, fungsi dari khitan adalah mempermudah dan mempercepat proses pembersihan fisik sebagai salah satu syarat sahnya ibadah.<br><br></div><div>Sedangkan secara medis, khitan mempunyai faedah yang sangat penting, yakni untuk membuang bagian anggota tubuh yang menjadi persembunyian kotoran, virus, bakteri, dan lainnya yang dapat membahayakan kesehatan.<br><br></div><div>“Saya mengapresiasi Baznas Sukoharjo atas terselenggaranya khitan massal ini dan semoga kegiatan ini dapar terus dijadikan agenda rutin tahunan,” ujar Bupati.<br><br></div><div>Etik Suryani menambahkan, dengan khitan massal masyarakat mendapatkan manfaat ganda, yakni bermanfaat untuk mendidik anak-anak agar berperilaku hidup bersih dan sehat, serta merupakan bagian dari pemanfaatan bantuan dana zakat atau infak yang dikelola Baznas dengan cara kreatif.<br><br></div><div>“Potensi zakat jika dihimpun sangat besar dan dapat dimanfaatkan sebesar-besarnya untuk kepentingan umat Islam. Bukan saja dengan cara konsumtif tradisional dibagi habis kepada para mustahik, tetapi bisa dengan cara kreatif dan produktif seperi khitan massal ini,” pungkas Etik.<br><br></div>',
            'user_id' => 1,
            'created_at' => '2023-02-09 13:30:45'
        ],
        [
            'judul' => 'Baznas Sukoharjo Salurkan Bantuan Kepada 19 Warga Tidak Mampu',
            'slug' => 'baznas-sukoharjo-salurkan-bantuan-kepada-19-warga-tidak-mampu',
            'image' => '20230218031340_berita2.jpeg',
            'body' => '<div><strong>SUKOHARJO </strong>– Badan Amil Zakat Nasional (Baznas) Kabupaten Sukoharjo kembali menyalurkan bantuan untuk masyarakat Sukoharjo kurang mampu. Kali ini, bantuan diberikan kepada 39 orang dari sejumlah kecamatan. Bantuan diserahkan langsung oleh Bupati Sukoharjo, Etik Suryani di Lobi Kantor Bupati, Kamis (15/9/2022).<br><br></div><div>Bantuan yang disalurkan sendiri terdiri antara lain untuk biaya pengobatan, bantuan biaya hidup, bantuan modal usaha, bantuan kursi roda, gerobak hik, dan lainnya. Nilai bantuan yang diberikan sendiri bervariasi mulai Rp1 juta hingga Rp3,5 juta.</div><div><br>“Selama ini bantuan untuk msyarakat kurang mampu rutin diberikan melalui Baznas Sukoharjo. Sebelumnya, warga tersebut mengajukan permohonan bantuan kepada Bupati dan ditindaklanjuti oleh Baznas yang bisa mencairkan bantuan dengan cepat,” terang Bupati.</div><div><br>Bupati melanjutkan, bantuan yang disalurkan sesuai pengajuan yang masuk ke Bupati. Untuk itu, Bupati meminta kepada masyarakat yang membutuhkan bantuan untuk mengajukan langsung ke Bupati. “Tidak perlu dibawah kemana-mana. Pasti saya respon dan tindak lanjuti,” tegasnya.</div><div><br>Bupati juga meminta semua camat dan kepala desa (kades) atau lurah jika ada warganya yang butuh bantuan bisa diajukan. Baik itu bantuan rehab rumah, bantuan pendidikan, pengobatan, modal usaha, dan lainnya.</div><div><br>“Tidak usah diuplod di medsos biar viral, langsung ajukan permohonan ke saya, pasti saya tindak lanjuti melalui Baznas,” tandasnya.</div><div><br>Bupati juga mengatakan, bantuan tersebut diharapkan dapat meringankan beban masyarakat kurang mampu di Kabupaten Sukoharjo yang tengah membutuhkan. Bantuan yang diberikan tersebut diharapkan dapat dimanfaatkan dengan baik.</div><div><br>Sementara itu, Ketua Baznas Kabupaten Sukoharjo, Sardiyono, menyampaikan tahun ini Baznas Sukoharjo sudah beberapa kali menyalurkan bantuan untuk masyarakat yang membutuhkan. Pemberian bantuan sendiri rutin dilakukan Baznas untuk meringankan beban masyarakat kurang mampu.<br><br></div>',
            'user_id' => 1,
            'created_at' => '2023-02-10 13:30:45'
        ],
        [
            'judul' => 'Bupati Serahkan Bantuan dari Baznas kepada 57 Warga Kurang Mampu',
            'slug' => 'bupati-serahkan-bantuan-dari-baznas-kepada-57-warga-kurang-mampu',
            'image' => '20230218044836_berita3.jpeg',
            'body' => '<div><strong>SUKOHARJO</strong> – Sebanyak 57 warga kurang mampu kembali mendapat bantuan dari Badan Amil Zakat Nasional (Baznas) Kabupaten Sukoharjo. Bantuan diberikan dalam berbagai bentuk seperti bantuan pengobatan, pendidikan, modal usaha, rehab RTLH dan lainnya. Penyerahan bantuan dilakukan oleh Bupati Sukoharjo Etik Suryani di Lobi Kantor Bupati, Kamis (15/12/2022).<br><br></div><div>“Bantuan ini bersumber dari Baznas Sukoharjo. Saat ini memang permohonan bantuan langsung ditindaklanjuti Baznas agar bisa segera direalisasikan,” ujar Bupati.<br><br></div><div>Bupati berharap bantuan yang diberikan dapat meringankan beban masyarakat kurang mampu di Kabupaten Sukoharjo. Menurutnya, warga yang membutuhkan bantuan bisa langsung mengajukan ke Bupati yang nantinya diteruskan ke Baznas untuk ditindaklanjuti.<br><br></div><div>Sementara itu, Ketua Baznas Sukoharjo, Sardiyono, mengatakan ada 57 orang menerima bantuan dengan total nilai bantuan sebesar Rp338 juta. Menurutnya, nilai bantuan bervariasi mulai Rp1 juta hingga Rp20 juta. Bantuan yang diberikan antara lain bantuan pengobatan, bantuan biaya hidup, bantuan modal usaha, bantuan gerobak UMKM, hingga bantuan rehab RTLH.</div><div><br>“Disatu sisi kami menerima banyak zakat, infak dan sedekah dari berbagai pihak dan dana yang terhimpun disalurkan kembali kepada masyarakat yang membutuhkan,” katanya.<br><br></div><div>Sardiyono juga mengatakan, peran aktif pemerintah desa, kelurahan dan kecamatan sudah dilakukan setelah ada imbauan dari Bupati Sukoharjo. Permohonan bantuan yang masuk kemudian diverifikasi dan dilakukan kunjungan ke lokasi calon penerima.<br><br></div>',
            'user_id' => 1,
            'created_at' => '2023-02-11 13:30:45'
        ]]);
    }
}
