<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PenerimazisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('penerimazis')->insert([[
            'bulan' => 'Januari',
            'tahun' => '2022',
            'zakat' => '10000000',
            'infaq_terikat' => '20000000',
            'infaq_umum' => '30000000',
            'total' => '60000000',
            'user_id' => 1,
        ]]);
    }
}
