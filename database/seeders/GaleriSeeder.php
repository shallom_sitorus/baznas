<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class GaleriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('galeri')->insert([[
            'judul' => 'Bantuan Sosial',
            'deskripsi' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis dicta cum harum aut voluptate praesentium eveniet deserunt itaque nulla nobis.',
            'tanggal' => '2023-03-01',
        ]]);

        DB::table('galeri_image')->insert([[
            'galeri_id' => 1,
            'foto' => '20230309023555_berita2.jpeg'
        ]]);
    }
}
